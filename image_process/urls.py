"""image_process URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
    1. Import the include() function: from django.urls import include, path
Including another URLconf
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.contrib.staticfiles import views
from django.conf import settings
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic.base import TemplateView

# index_path = os.path.join(os.path.dirname(os.path.abspath("__file__")), 'static/index.html')
# print("index_path", index_path)

urlpatterns = [
    # url(r'^$', TemplateView.as_view(template_name=index_path),
    #     name='home'),

    url(r'^image/', include('images.urls')),

    # url(r'^static/(?P<path>.*)$', views.serve),
]

# if settings.DEBUG:
#     media_root = os.path.join(settings.BASE_DIR, 'media2')
#     urlpatterns += static('/media2/', document_root=media_root)
