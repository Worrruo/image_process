from .celery import app as celery_app
import os
import logging

__all__ = ['celery_app']

logger = logging.getLogger('image_process')
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
logger.info(f"image base_dir: {BASE_DIR}")
