from django.db import models


class Base(models.Model):
    STATUS_CHOICES = (
        (0, '删除'),
        (1, '生效'),
    )
    created_time = models.DateTimeField('创建时间', auto_now_add=True, blank=True, null=True)
    updated_time = models.DateTimeField('更新时间', auto_now=True, blank=True, null=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=0)

    def __str__(self):
        r = []
        for k, v in self.__dict__.items():
            if not k.startswith("_"):
                r.append(f"{k}: {v}")
        return self.__class__.__name__ + "\n".join(r)


class User(Base):
    name = models.CharField('姓名', max_length=10, blank=False)


class Image(Base):
    path_url = models.CharField('图片路径',  max_length=255, blank=False, null=False, unique=True)
