from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from images.views import ImageUploadView, ImageMergeView, index

urlpatterns = [
    url("", index, name="image_index"),
    url(r'^post$', ImageUploadView.as_view()),
    url(r'^merge$', ImageMergeView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
