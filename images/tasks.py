import os
from image_process import celery_app, logger, BASE_DIR
from PIL import Image


@celery_app.task(bind=True)
# @celery_app.task(name="image", default_retry_delay=5, max_retries=3, bind=True)
def merge_image(image_name, image_list):
    image_path = os.path.join(BASE_DIR, "static/image")
    new_image_path = os.path.join(image_path, image_name)

    image_list = [os.path.join(image_path, i) for i in image_list]
    images = []
    for image in image_list:
        images.append(Image.open(image).convert("RGB"))
    logger.info(f"image_list {image_list} {images}")

    width, height = 0, 0
    for image in images:
        logger.info(f'image, {image}')
        w, h = image.size
        height += h
        if width < w:
            width = w
    new_image = Image.new("RGB", (width, height))

    y_offset = 0
    for image in images:
        new_image.paste(image, (0, y_offset))
        logger.info(f"y_offset {y_offset}")
        y_offset += image.size[1]
    logger.info("new_image_path {image_name} {new_image_path}")
    new_image.save(new_image_path, "WEBP")
# except Exception as e:
#     # 隔5S重试，最多3次
#     logger.info(f"merge image error: {e}")
#     raise self.retry(exc=e)

# @task(name="task_a", default_retry_delay=5, max_retries=3, bind=True)
# def task_a(self):
#     try:
#         print("in task_a")
#     except Exception as e:
#         # 隔5S重试，最多3次
#         logger.info(str(e))
#         raise self.retry(exc=e)
#
#
# @periodic_task(
#     run_every=(crontab(minute='*/1')),
#     name="task_b",
#     ignore_result=True, bind=True)
# def task_b(self):
#     # 每1分钟执行一次
#     print("in task b")
