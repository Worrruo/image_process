import os
import uuid
from django.shortcuts import render_to_response
from django.http import HttpResponse
from PIL import Image
from rest_framework import viewsets, status, generics, mixins
from rest_framework.response import Response
from image_process.settings import HOST
from image_process import logger, BASE_DIR
from images import merge_image


# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# logger.info(f"image base_dir: {BASE_DIR}")

def index(request):
    index_path = os.path.join(BASE_DIR, 'static/index.html')
    print("index_path", index_path)
    with open(index_path) as f:
        d = f.read()
        return HttpResponse(d)


class ImageUploadView(generics.GenericAPIView):
    def post(self, request):
        """
        用户上传图片
        """
        resp = []
        files = request.FILES.getlist('file', None)
        logger.info(f"image upload file {files} {BASE_DIR}")
        image_path = os.path.join(BASE_DIR, "static/image")
        for file in files:
            name = str(uuid.uuid4())
            file_path = os.path.join(image_path, name)
            url = "static/image/" + name
            with open(file_path, "wb+") as image:
                for chunk in file.chunks():
                    image.write(chunk)
            data = {
                "name": name,
                "url": HOST + url
            }
            logger.info(f"image file {data}")
            resp.append(data)
        return Response(resp)


class AsyncImageMergeView(generics.GenericAPIView):

    def post(self, request):
        """
        合成
        """
        image_list = request.data["url_list"]
        logger.info(f"merge image {image_list}")
        image_name = ""
        for i in image_list:
            image_name += i
        image_name += ".webp"
        url = "static/image/" + image_name

        merge_image.delay()

        resp = {
            "name": image_name,
            "url": HOST + url
        }
        return Response(resp)


def TestMerge(idx):
    """test"""
    image_list = ["avatar1.jpeg", "avatar2.jpeg"]
    logger.info(f"merge image {image_list}")
    image_name = "test" + str(idx) + ".webp"
    url = "static/image/" + image_name
    logger.info(f"text before {idx}")
    merge_image.delay()
    resp = {
        "name": image_name,
        "url": HOST + url
    }
    logger.info(f"text after {idx}")
    return resp


class ImageMergeView(generics.GenericAPIView):
    def merge_image(self):
        pass

    def post(self, request):
        """
        合成
        """
        image_path = os.path.join(BASE_DIR, "static/image")
        image_list = request.data["url_list"]
        logger.info(f"merge image {image_list}")
        image_name = ""
        for i in image_list:
            image_name += i
        image_name += ".webp"
        new_image_path = os.path.join(image_path, image_name)
        url = "static/image/" + image_name

        image_list = [os.path.join(image_path, i) for i in image_list]
        images = []
        for image in image_list:
            images.append(Image.open(image).convert("RGB"))
        print("image_list", image_list, images)

        width, height = 0, 0
        for image in images:
            print('image', image)
            w, h = image.size
            height += h
            if width < w:
                width = w
        new_image = Image.new("RGB", (width, height))

        y_offset = 0
        for image in images:
            new_image.paste(image, (0, y_offset))
            print("y_offset", y_offset)
            y_offset += image.size[1]

        print("new_image_path", image_name, new_image_path)
        new_image.save(new_image_path, "WEBP")

        resp = {
            "name": image_name,
            "url": HOST + url
        }
        return Response(resp)
