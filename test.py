import os
from images.views import TestMerge
BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def index():
    index_path = os.path.join(BASE_DIR, 'static/index.html')
    print("index_path", index_path)
    with open(index_path, 'r') as f:
        r = f.read()
        print("r", r)


# index()

def test_celery():
    for i in range(3):
        r = TestMerge(i)
        print(i, r["url"])


def test():
    test_celery()
    pass


if __name__ == '__main__':
    test()
