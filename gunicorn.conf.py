import os
import gevent.monkey

gevent.monkey.patch_all()

import multiprocessing

debug = True
proc_name = 'wor_syj_gunicorn_proj'  # 进程名

pidfile = 'logs/gunicorn.pid'
logfile = 'logs/debug.log'

loglevel = 'debug'
bind = "127.0.0.1:8000"  # 绑定的ip与端口

# 启动的进程数
workers = multiprocessing.cpu_count() * 2 + 1
# worker_class = 'gunicorn.workers.ggevent.GeventWorker'
worker_class = 'gevent'

# x_forwarded_for_header = 'X-FORWARDED-FOR'
